# How to access VRE ?

To access the ODATIS VRE, simply click on the following link: https://jupyterhub.ifremer.fr

A login page appears, and you need to enter your Ifremer intranet account credentials.

```{note}
If you do not have an Ifremer login, please contact ???
```

```{image} ../images/vre-login.png
:alt: vre-login
:align: center
```

Once you've logged in, you'll be prompted to choose the configuration of your instance on the VRE. You can select the resources you wish to use from a list of several proposals. 


```{image} ../images/vre-options.png
:alt: vre-options
:align: center
```

Finally, once you've selected your server options, you'll come to a final window corresponding to your JupyterLab, in other words, a space where you'll be able to: build, discover, search, ...

```{image} ../images/vre-lab.png
:alt: vre-lab
:align: center
```