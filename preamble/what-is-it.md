# What is a VRE ? 

A VRE for **"Virtual Research Environment "** is a platform designed to facilitate collaborative research and data analysis in a scientific or academic context. 

VRE offer a set of tools, resources and functionalities to enable researchers to work together on research projects, share data, analyze data and collaborate remotely.

This guide provides an overview and explains the key features of the Odatis VRE.