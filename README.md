# Jupyterbook Test

Voici un Jupyterbook de test, pour étudier toutes les étapes du notebook, allant de création de la documentation à partir du notebook, jusqu'à son execution sur une session jupyter lab, en passant par sa compilation au format HTML.

- Documentation: https://odatis.gitlab-pages.ifremer.fr/jupyterbook-test/
- Notebook (Jupyterbook): https://odatis.gitlab-pages.ifremer.fr/jupyterbook-test/use/matplotlib.html
- Notebook (compilé en HTML): https://odatis.gitlab-pages.ifremer.fr/jupyterbook-test/notebook/matplotlib.html

## Installation

- Utiliser l'environnement `jb-test`
- Pour contruire le book: `jb build --all jupyterbook-test`
- Pour supprimer le build: `jb clean jupyterbook-test`

<!-- ## Evolutions

Je n'arrive pas à accéder à mes espaces disque datarmor depuis gitlab-ci, du coup le script odatis.ipynb ne trouve pas les données OSISAF, et je n'arrive pas à enregistrer les notebooks au format HTML sur un dossier en particulier

## Branches

La branche `main` contient le code principales, lorsque je souhaite déployer la nouvelle documentation, et que le job `test` est passé avec succès sur cette branche, je peux faire un merge request de la branche `main` vers la branche `pages`, alors le job `deploy` se chargera de déployer la doc. Pour celà je me place sur la branche `pages` et je fais merge from `main`. 
Pour cela sur VSC se placer sur la branche `pages`, sélectionner `branch/merge` et selectionner from `main`.

## Noteook HTML

Dans le fichier gitlab-ci, le script nbconvert_test.py compile le notebook en format html et le place dans le dossier _build/html/notebook, ensuite on peux accéder aux notebooks compilé par le lien:
- https://odatis.gitlab-pages.ifremer.fr/jupyterbook-test/notebook/matplotlib.html -->