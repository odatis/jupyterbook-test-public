from nbconvert.preprocessors import ExecutePreprocessor
from nbconvert import HTMLExporter
import nbformat
import pathlib
import json

if __name__ == "__main__":
    
    file_path = '_toc.yml'
    target = '- file: '
    paths = []
    with open(file_path, 'r') as file:
        for line in file:
            if target in line.strip():
                path = line.strip()[len(target):]
                file_path = pathlib.Path(f'{path}.ipynb')
                if file_path.exists():
                    paths.append(file_path.as_posix())
    
    for path in paths:
        with open(path, 'r') as file:
            notebook = nbformat.reads(json.dumps(json.load(file)), as_version=4)
        
        file = path.split('.')[0].split('/')[-1]
        
        ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
        try:
            ep.preprocess(notebook, {'metadata': {'path': './'}})
        except Exception as e:
            print(f'Erreur lors de l\'exécution du notebook: {e}')
    
        html_exporter = HTMLExporter(template_name="classic")
        (body, resources) = html_exporter.from_notebook_node(notebook)

        pathlib.Path("./_build/html/notebook").mkdir(parents=True, exist_ok=True)

        with open(f'./_build/html/notebook/{file}.html', 'w', encoding='utf-8') as f:
            f.write(body)